#!/bin/bash
#Script to assist with the installation of a Noteworthy Hub for a Debian/Ubuntu based system

#color codes
RED='\033[1;31m'
YELLOW='\033[1;33m'
BLUE="\\033[38;5;27m"
SEA="\\033[38;5;49m"
GREEN='\033[1;32m'
CYAN='\033[1;36m'
NC='\033[0m'

#emoji codes
CHECK_MARK="${GREEN}\xE2\x9C\x94${NC}"
X_MARK="${RED}\xE2\x9C\x96${NC}"
PIN="${RED}\xF0\x9F\x93\x8C${NC}"
CLOCK="${GREEN}\xE2\x8C\x9B${NC}"
ARROW="${SEA}\xE2\x96\xB6${NC}"
BOOK="${RED}\xF0\x9F\x93\x8B${NC}"
HOT="${ORANGE}\xF0\x9F\x94\xA5${NC}"
WARNING="${RED}\xF0\x9F\x9A\xA8${NC}"
dversion="1.0"

function install_docker(){

echo -e "${GREEN}1. Install Docker${NC}"
echo -e "${YELLOW}================================================================${NC}"

if [[ "$USER" != "root" ]]
then
    echo -e "${CYAN}You are currently logged in as ${GREEN}$USER${NC}"
    echo -e "${CYAN}Please switch to the root account.${NC}"
    echo -e "${CYAN}e.g. ${GREEN}sudo su -${NC}"
    echo -e "${YELLOW}================================================================${NC}"
    echo -e "${NC}"
    exit
fi

usernew="$(whiptail --title "NOTEWORTHY HUB INSTALLER $dversion" --inputbox "Enter your username" 8 72 3>&1 1>&2 2>&3)"
echo -e "${ARROW} ${YELLOW}Creating new user...${NC}"
adduser --gecos "" "$usernew"
usermod -aG sudo "$usernew"
echo -e "${NC}"
echo -e "${ARROW} ${YELLOW}Update and upgrade system...${NC}"
apt update -y && apt upgrade -y
echo -e "${ARROW} ${YELLOW}Installing docker...${NC}"

if [[ $(lsb_release -d) = *Debian* ]]
then

sudo apt-get remove docker docker-engine docker.io containerd runc -y > /dev/null 2>&1 
sudo apt-get update -y  > /dev/null 2>&1
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y > /dev/null 2>&1
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add - > /dev/null 2>&1
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable" > /dev/null 2>&1
sudo apt-get update -y  > /dev/null 2>&1
sudo apt-get install docker-ce docker-ce-cli containerd.io -y > /dev/null 2>&1  

else

sudo apt-get update -y > /dev/null 2>&1
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y > /dev/null 2>&1 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - > /dev/null 2>&1
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" > /dev/null 2>&1
sudo apt-get update -y > /dev/null 2>&1 
sudo apt-get install docker-ce docker-ce-cli containerd.io -y > /dev/null 2>&1  

fi

echo -e "${ARROW} ${YELLOW}Adding $usernew to docker group...${NC}"
adduser "$usernew" docker > /dev/null 2>&1
echo -e "${NC}"
echo -e "${YELLOW}=====================================================${NC}"
echo -e "${YELLOW}Running through some checks...${NC}"
echo -e "${YELLOW}=====================================================${NC}"

if sudo docker run hello-world > /dev/null 2>&1
then
	echo -e "${CHECK_MARK} ${CYAN}Docker is installed.${NC}"
else
	echo -e "${X_MARK} ${CYAN}Docker did not install.${NC}"
fi

if [[ $(getent group docker | grep "$usernew") ]] 
then
	echo -e "${CHECK_MARK} ${CYAN}User $usernew is member of 'docker'${NC}"
else
	echo -e "${X_MARK} ${CYAN}User $usernew is not member of 'docker'${NC}"
fi

echo -e "${YELLOW}=====================================================${NC}"
echo -e "${NC}"

searchString="notectl()"
file="/etc/bash.bashrc"
if grep -Fq "$searchString" $file ; then
echo -e "Noteworthy already added to /etc/bash.bashrc"
else
echo -e "${ARROW} ${YELLOW}Adding notectl function to /etc/bash.bashrc...${NC}"
echo '#noteworthy
notectl() {
        docker run --rm -it -v "/var/run/docker.sock:/var/run/docker.sock" decentralabs/noteworthy:taproot-beta "\$@";
}' >> /etc/bash.bashrc
fi

echo -e "Switching to new user account..."
echo -e "${CYAN}To install Noteworthy Client, please re-run this script as $usernew and choose option 2.${NC}"
echo -e "${NC}"
su - $usernew

}

function install_noteworthy_client() {
echo -e "${GREEN}2. Install Noteworthy Client${NC}"
echo -e "${YELLOW}================================================================${NC}"

searchString="notectl()"
file="/home/$USER/.bashrc"
if grep -Fq "$searchString" $file ; then
echo -e "Noteworthy already added to ~/.bashrc."
else
echo -e "${ARROW} ${YELLOW}Adding notectl function to ~/.bashrc...${NC}"
echo '#noteworthy
notectl() {
        docker run --rm -it -v "/var/run/docker.sock:/var/run/docker.sock" decentralabs/noteworthy:taproot-beta "\$@";
}' >> ~/.bashrc
fi

read -p "Are you using your own Noteworthy Hub Y/N?" -n 1 -r
echo -e "${NC}"
if [[ $REPLY =~ ^[Yy]$ ]]
        then
        echo -n "Please enter your complete hub domain, e.g. myhub.com : "
        read name1
        echo -n "Please enter the complete domain for this client, e.g. myclient.com : "
        read name2
        notectl install --hub $name1 --domain $name2
else
        echo -e "${CYAN}Before installing, you must have a valid invite key.${NC}"
        read -p "Do you have an invite key Y/N?" -n 2 -r
        echo -e "${NC}"
        if [[ $REPLY =~ ^[Yy]$ ]]
                then
                echo -n "Please enter the domain for this client, e.g. myclient.com : "
                read name2
                echo -e "${ARROW} ${YELLOW}Installing for default hub...${NC}"
                notectl install --domain $name2
        else
		echo -e "${YELLOW}Please obtain an invite key from the Noteworthy team, then re-run this script.${NC}"
                quit
        fi
fi
}

function notectl() {
        docker run --rm -it -v "/var/run/docker.sock:/var/run/docker.sock" decentralabs/noteworthy:taproot-beta $@;
}


if ! figlet -v > /dev/null 2>&1
then
sudo apt-get update -y > /dev/null 2>&1
sudo apt-get install -y figlet > /dev/null 2>&1
fi

clear
sleep 1
echo -e "${BLUE}"
figlet -f slant "Noteworthy Client"
echo -e "${YELLOW}================================================================${NC}"
echo -e "${GREEN}Version: $dversion${NC}"
echo -e "${GREEN}OS: Ubuntu 16.04/18.04/20.04, Debian 9.12/10.3${NC}"
echo -e "${GREEN}Created by: Goose-Tech from Zel's team${NC}"
echo -e "${GREEN}Special thanks to XK4MiLX${NC}"
echo -e "${YELLOW}================================================================${NC}"
echo -e "${CYAN}1 - Install Docker${NC}"
echo -e "${CYAN}2 - Install Noteworthy Client${NC}"
echo -e "${YELLOW}================================================================${NC}"

read -p "Pick an option: " -n 1 -r

  case "$REPLY" in

 1)  
    clear
    sleep 1
    install_docker
 ;;
 2) 
    clear
    sleep 1
    install_noteworthy_client
 ;;
   esac