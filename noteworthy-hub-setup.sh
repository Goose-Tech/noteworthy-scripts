#!/bin/bash
#Script to assist with the installation of a Noteworthy Hub for a Debian/Ubuntu based system

#color codes
RED='\033[1;31m'
YELLOW='\033[1;33m'
BLUE="\\033[38;5;27m"
SEA="\\033[38;5;49m"
GREEN='\033[1;32m'
CYAN='\033[1;36m'
NC='\033[0m'

#emoji codes
CHECK_MARK="${GREEN}\xE2\x9C\x94${NC}"
X_MARK="${RED}\xE2\x9C\x96${NC}"
PIN="${RED}\xF0\x9F\x93\x8C${NC}"
CLOCK="${GREEN}\xE2\x8C\x9B${NC}"
ARROW="${SEA}\xE2\x96\xB6${NC}"
BOOK="${RED}\xF0\x9F\x93\x8B${NC}"
HOT="${ORANGE}\xF0\x9F\x94\xA5${NC}"
WARNING="${RED}\xF0\x9F\x9A\xA8${NC}"
dversion="1.0"

function install_docker(){

echo -e "${GREEN}1. Install Docker${NC}"
echo -e "${YELLOW}================================================================${NC}"

if [[ "$USER" != "root" ]]
then
    echo -e "${CYAN}You are currently logged in as ${GREEN}$USER${NC}"
    echo -e "${CYAN}Please switch to the root account.${NC}"
    echo -e "${YELLOW}================================================================${NC}"
    echo -e "${NC}"
    exit
fi

usernew="$(whiptail --title "NOTEWORTHY HUB INSTALLER $dversion" --inputbox "Enter your username" 8 72 3>&1 1>&2 2>&3)"
echo -e "${ARROW} ${YELLOW}Creating new user...${NC}"
adduser --gecos "" "$usernew"
usermod -aG sudo "$usernew"
echo -e "${NC}"
echo -e "${ARROW} ${YELLOW}Update and upgrade system...${NC}"
apt update -y && apt upgrade -y
echo -e "${ARROW} ${YELLOW}Installing docker...${NC}"

if [[ $(lsb_release -d) = *Debian* ]]
then

sudo apt-get remove docker docker-engine docker.io containerd runc -y > /dev/null 2>&1 
sudo apt-get update -y  > /dev/null 2>&1
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y > /dev/null 2>&1
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add - > /dev/null 2>&1
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable" > /dev/null 2>&1
sudo apt-get update -y  > /dev/null 2>&1
sudo apt-get install docker-ce docker-ce-cli containerd.io -y > /dev/null 2>&1  

else

sudo apt-get update -y > /dev/null 2>&1
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y > /dev/null 2>&1 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - > /dev/null 2>&1
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" > /dev/null 2>&1
sudo apt-get update -y > /dev/null 2>&1 
sudo apt-get install docker-ce docker-ce-cli containerd.io -y > /dev/null 2>&1  

fi

echo -e "${ARROW} ${YELLOW}Adding $usernew to docker group...${NC}"
adduser "$usernew" docker > /dev/null 2>&1
echo -e "${NC}"
echo -e "${YELLOW}=====================================================${NC}"
echo -e "${YELLOW}Running through some checks...${NC}"
echo -e "${YELLOW}=====================================================${NC}"

if sudo docker run hello-world > /dev/null 2>&1
then
	echo -e "${CHECK_MARK} ${CYAN}Docker is installed.${NC}"
else
	echo -e "${X_MARK} ${CYAN}Docker did not install.${NC}"
fi

if [[ $(getent group docker | grep "$usernew") ]] 
then
	echo -e "${CHECK_MARK} ${CYAN}User $usernew is member of 'docker'${NC}"
else
	echo -e "${X_MARK} ${CYAN}User $usernew is not member of 'docker'${NC}"
fi

echo -e "${YELLOW}=====================================================${NC}"
echo -e "${NC}"
echo -e "${ARROW} ${YELLOW}Adding notectl-hub-dev function to /etc/bash.bashrc...${NC}"
echo '#noteworthy
notectl-hub-dev() {
	docker run --rm -it -v "/var/run/docker.sock:/var/run/docker.sock" decentralabs/noteworthy:hub-dev "$@";
}' >> /etc/bash.bashrc
echo -e "Switching to new user account..."
echo -e "${CYAN}To install Noteworthy Hub, please re-run this script as $usernew and choose option 2.${NC}"
echo -e "${NC}"
su - $usernew

}

function install_noteworthy_dev_hub() {

echo -e "${GREEN}2. Install Noteworthy Developer Hub${NC}"
echo -e "${YELLOW}================================================================${NC}"

if [[ "$USER" == "root" ]]
then
    echo -e "${CYAN}You are currently logged in as ${GREEN}$USER${NC}"
    echo -e "${CYAN}For security purposes, it is not recommended to install as root.${NC}"
    echo -e "${YELLOW}=================================================================${NC}"
    echo -e "${NC}"
        read -p "Would you like switch to user account Y/N?" -n 1 -r
        echo -e "${NC}"
        if [[ $REPLY =~ ^[Yy]$ ]]
        then
        read -p "Enter your username: " USERNAME
        echo -e "${CYAN}To install Noteworthy Hub, please re-run this script as $USERNAME and choose option 2.${NC}"
        sudo su - $USERNAME
fi
    exit
fi

echo -e "${ARROW} ${YELLOW}Installing compiler...${NC}"
sudo apt-get update -y  > /dev/null 2>&1
sudo apt-get install make -y > /dev/null 2>&1  
if [ -d /home/$USER/noteworthy ]; then
	echo -e "${ARROW} ${CYAN}Existing noteworthy repo found. Replacing.${NC}"
	sudo rm -rf noteworthy  > /dev/null 2>&1 && sleep 2
    sudo rm -rf noteworthy  > /dev/null 2>&1 && sleep 2
fi
echo -e "${ARROW} ${YELLOW}Cloning noteworthy repo...${NC}"
cd ~
git clone https://github.com/decentralabs/noteworthy.git
cd ~/noteworthy

echo -e "${ARROW} ${YELLOW}Building hub container...${NC}"
make docker ROLE=hub GIT_COMMIT=$(git rev-parse HEAD) RELEASE_TAG=dev
echo -e "${ARROW} ${YELLOW}Building link container...${NC}"
make docker ROLE=link RELEASE_TAG=dev

echo -e "${CYAN}Hub installed. To configure, please re-run this script and choose option 3.${NC}"
exec bash

}

function initial_hub_configuration() {
echo -e "${GREEN}3. Initial Hub Configuration${NC}"
echo -e "${YELLOW}================================================================${NC}"

# search ~/.bashrc for noteworthy and add if not there
searchString="#noteworthy"
file="/home/$USER/.bashrc"
if grep -Fq "$searchString" $file ; then
echo -e "Noteworthy already added to ~/.bashrc."
else
echo -e "${ARROW} ${YELLOW}Adding notectl-hub-dev function to ~/.bashrc...${NC}"
echo '#noteworthy
notectl-hub-dev() {
        docker run --rm -it -v "/var/run/docker.sock:/var/run/docker.sock" decentralabs/noteworthy:hub-dev "$@";
}' >> /home/$USER/.bashrc
fi

read -p "Enter the full domain for your hub, e.g. hub.example.com: " FQDN

notectl-hub-dev launcher launch_hub $FQDN

#add line here to verify the hub was added correctly

}

function notectl-hub-dev() {
        docker run --rm -it -v "/var/run/docker.sock:/var/run/docker.sock" decentralabs/noteworthy:hub-dev "$@";
}

function generate_hub_invite() {
echo -e "${GREEN}4. Generate Hub Invite Key${NC}"
echo -e "${YELLOW}================================================================${NC}"
echo -n "Please enter subdomain to invite, e.g. chat : "
read name
hubcontainer=$(docker ps -aqf "name=noteworthy-launcher-hub-default")
echo -e "${ARROW} ${YELLOW}Generating key for $name...${NC}"
docker exec -it $hubcontainer notectl invite $name
echo -e "${CYAN}Copy the above key and use it to connect your client to this hub.${NC}"
}


if ! figlet -v > /dev/null 2>&1
then
sudo apt-get update -y > /dev/null 2>&1
sudo apt-get install -y figlet > /dev/null 2>&1
fi

clear
sleep 1
echo -e "${BLUE}"
figlet -f slant "Noteworthy Hub"
echo -e "${YELLOW}================================================================${NC}"
echo -e "${GREEN}Version: $dversion${NC}"
echo -e "${GREEN}OS: Ubuntu 16.04/18.04/20.04, Debian 9.12/10.3${NC}"
echo -e "${GREEN}Created by: Goose-Tech from Zel's team${NC}"
echo -e "${GREEN}Special thanks to XK4MiLX${NC}"
echo -e "${YELLOW}================================================================${NC}"
echo -e "${CYAN}1 - Install Docker${NC}"
echo -e "${CYAN}2 - Install Noteworthy Developer Hub${NC}"
echo -e "${CYAN}3 - Initial Hub Configuration${NC}"
echo -e "${CYAN}4 - Generate Hub Invite Key${NC}"
echo -e "${YELLOW}================================================================${NC}"

read -p "Pick an option: " -n 1 -r

  case "$REPLY" in

 1)  
    clear
    sleep 1
    install_docker
 ;;
 2) 
    clear
    sleep 1
    install_noteworthy_dev_hub
 ;;
 3) 
    clear
    sleep 1
    initial_hub_configuration
 ;;
 4)
 	clear
 	sleep 1
 	generate_hub_invite
 ;;
   esac
