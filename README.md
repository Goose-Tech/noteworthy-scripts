# Noteworthy Scripts

Scripts to help with installing Noteworthy Hub or Client

# Noteworthy Hub Setup v1.0
#### File: noteworthy-hub-setup.sh

A script with multiple menu options for installing and setting up a Noteworthy Developer Hub

## Options Include
1. Install Docker
2. Install Noteworthy Developer Hub
3. Initial Hub Configuration
4. Generate Hub Invite Key

To begin, login to the Debian/Ubuntu server as root and run the following command:
```
bash -i <(curl -s https://gitlab.com/Goose-Tech/noteworthy-scripts/-/raw/master/noteworthy-hub-setup.sh)
```
__This script will need to be re-run for each step__

# Steps
## 1. - Install Docker
This function checks whether or not the user is logged in as root. If yes, then it prompts to create a new sudo user.
Next, it performs update/upgrade and installs the following packages:
```
docker-ce
docker-ce-cli
containerd.io
```

It also adds the following function to /etc/bash.bashrc, which enables the use of `notectl-hub-dev` from the command line*.
```
#noteworthy
notectl-hub-dev() {
	docker run --rm -it -v "/var/run/docker.sock:/var/run/docker.sock" decentralabs/noteworthy:hub-dev "$@";
}
```
**Note: This will not work until after option 2 of the script has been run.*

## 2. - Install Noteworthy Developer Hub
This function checks whether or not the user is logged in as root. If so, it recommends switching to the user created in step 1. If not, then it will continue by cloning `https://github.com/decentralabs/noteworthy.git` repository into ~/noteworthy.
It then installs the package `make` and begins to build the hub and link containers*.

**Note: hub and link containers are currently being pulled from the dev branch. This will change in future releases.*

## 3. - Initial Hub Configuration
This function adds the above `notectl-hub-dev()` function to the user's ~/.bashrc, which enables the use of `notectl-hub-dev` from the command line.
Next, the function prompts the user to enter the fully qualified domain name (FQDN) for the hub, e.g. `hub.example.com`. After receiving the user's input, it passes the FQDN to the noteworthy hub docker container to launch the hub with the following command:

```
notectl-hub-dev launcher launch_hub $FQDN
```

## 4. - Generate Hub Invite Key
This function prompts the user to enter a single subdomain to generate an invite key, which may be used to connect a client to the hub, e.g. `mychatserver` 

Example Output:
```
mychatserver: b4cay30f-fc87-4e2b-a423-ffe91cd51ab7
```

To verify installation, visit your subdomain, e.g. hub.example.com. You should see welcome to nginx.

---

# Noteworthy Client Setup v1.0
#### File: noteworthy-client-setup.sh

A script with multiple menu options for installing and setting up a Noteworthy Client

## Options Include
1. Install Docker
2. Install Noteworthy Client

To begin, login to the Debian/Ubuntu server as root and run the following command:
```
bash -i <(curl -s https://gitlab.com/Goose-Tech/noteworthy-scripts/-/raw/master/noteworthy-client-setup.sh)
```
__This script will need to be re-run for each step__

# Options
## 1. - Install Docker
This function checks whether or not the user is logged in as root. If yes, then it prompts to create a new sudo user.
Next, it performs update/upgrade and installs the following packages:
```
docker-ce
docker-ce-cli
containerd.io
```

It also adds the following function to /etc/bash.bashrc, which enables the use of `notectl` from the command line*.
```
#noteworthy
notectl() {
	docker run --rm -it -v "/var/run/docker.sock:/var/run/docker.sock" decentralabs/noteworthy:taproot-beta "$@";
}
```
**Note: This will not work until after option 2 of the script has been run.*

# 2. - Install Noteworthy Client
This function adds the above `notectl()` function to the user's ~/.bashrc, which enables the use of `notectl` from the command line.
Next, the function prompts the user to enter the fully qualified domain name (FQDN) for the hub, e.g. `hub.example.com`. If not using a custom hub, then the option is provided to enter the subdomain registered at noteworthy.im. After receiving the user's input, it installs the corresponding docker containers using the domain names provided.

```
notectl install --hub $hubdomain --domain $clientdomain
```

To verify installation, visit matrix.subdomain.hubdomain, e.g. matrix.mychatserver.example.com or matrix.mychatserver.noteworthy.im.

A CNAME record may be used to direct your domain to the proper address.

---

For official documentation, please visit: https://github.com/decentralabs/noteworthy or https://noteworthy.tech/hub/